package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {



	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
		}
		return max;
	}
	
	public int getMin(IntegersBag bag){
		int min = Integer.MAX_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if(min>value){
					min = value;
				}
			}
		}
		return min;
	}
	
	public double getRandom(IntegersBag bag){
		double value = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){

				double random = Math.random()*4;
				int num = (int)random;

				double a = (double)iter.next(); 
				double b = 0;

				if(iter.hasNext()){
					b = (double)iter.next();
				}

				switch(num){
				case 1: value = a+b;
				break;

				case 2: value = a-b;
				break;

				case 3: value = a*b;		  
				break;

				case 4: 
					if(b == 0){
						value += 0;
					}
					else{
						value = a/b;
					}  
				}
			}
		}
		return value;
	}
	
	public int getCountOfNums(IntegersBag bag){
		int count = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				count++;
				iter.next();
			}
		}
		return count;
	}
}
